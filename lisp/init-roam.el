;;; init-roam.el --- Roam configuration file -*- lexical-binding: t -*-
;;; Commentary:

;;; Code:

(require-package 'org-roam)
(require-package 'org-roam-ui)

(require 'org-roam)

(setq org-roam-directory (file-truename "~/notes"))
(global-set-key (kbd "C-c n l") 'org-roam-buffer-toggle)
(global-set-key (kbd "C-c n f") 'org-roam-node-find)
(global-set-key (kbd "C-c n i") 'org-roam-node-insert)
(global-set-key (kbd "C-c n c") 'org-roam-capture)

;; The following definition is required by customizing display template
(cl-defmethod org-roam-node-type ((node org-roam-node))
  "Return the TYPE of NODE."
  (condition-case nil
      (file-name-nondirectory
       (directory-file-name
        (file-name-directory
         (file-relative-name (org-roam-node-file node) org-roam-directory))))
    (error "")))
(setq org-roam-node-display-template
      (concat "${type:15} ${title:*} "
              (propertize "${tags:10}" 'face 'org-tag)))

;; Automatically rebuild the roam database
(org-roam-db-autosync-mode)

;; Templates that will be prompted when new roam nodes are created
(setq org-roam-capture-templates
      '(("a" "Agenda" plain "%?"
         :if-new (file+head "agenda/${slug}.org" "#+title: ${title}\n#+options: ^:nil \\n:t arch:t prop:t\n#+filetags: :agenda:\n#+startup: overview\n\n** Items\n")
         :immediate-finish t
         :unnarrowed t)
        ("p" "Projects" plain "%?"
         :if-new (file+head "agenda/projects/${slug}.org" "#+title: ${title}\n#+options: ^:nil \\n:t arch:t prop:t\n#+filetags: :project:\n#+startup: overview\n\n** References\n")
         :immediate-finish t
         :unnarrowed t)
        ("r" "Records" plain "%?"
         ;; Notes extracted with explicit sources/references. It's part of the
         ;; major concepts of the Zettelkasten methodology.
         :if-new (file+head "record/${slug}.org" "#+title: ${title}\n#+options: ^:nil \\n:t arch:t prop:t\n#+filetags: :record:\n#+startup: overview\n\n** References\n")
         :immediate-finish t
         :unnarrowed t)
        ("b" "Bugs" plain "%?"
         :if-new (file+head "bug/${slug}.org" "#+title: ${title}\n#+options: ^:nil \\n:t arch:t prop:t\n#+filetags: :bug:\n#+startup: showall\n\n** Bug Info\n")
         :immediate-finish t
         :unnarrowed t)
        ("h" "HowTo" plain "%?"
         :if-new (file+head "howto/${slug}.org" "#+title: ${title}\n#+options: ^:nil \\n:t arch:t prop:t\n#+filetags: :howto:\n#+startup: showall\n\n** Introduction\n")
         :immediate-finish t
         :unnarrowed t)))


(provide 'init-roam)
;;; init-roam.el ends here
