;;; init-mu4e.el --- Mu4e configuration file -*- lexical-binding: t -*-
;;; Commentary:

;;; Code:

;; Default location for linux (Fedora specifically)
(add-to-list 'load-path "/usr/share/emacs/site-lisp/mu4e")

;; Use the `require' keyword to load a module into Emacs runtime
(require 'mu4e)


;; use `mu4e' as the default mail client for emacs, ironic, huh?
(setq mail-user-agent 'mu4e-user-agent)

(setq user-mail-address "shichen@redhat.com"
      user-full-name "Shizhao Chen")

(setq mu4e-maildir "~/.mail/redhat/"
      mu4e-drafts-folder "/redhat/[Gmail]/Drafts"
      mu4e-sent-folder   "/redhat/[Gmail]/Sent Mail"
      mu4e-trash-folder  "/redhat/[Gmail]/Trash")

(setq mu4e-get-mail-command "mbsync --verbose redhat")

;; TODO there must is a smarter way to extend a list with a list
(add-to-list 'mu4e-bookmarks
             '( :name "TOME Unread"
                 :query "maildir:/redhat/Inbox AND to:shichen@redhat.com"
                 :key ?m))

(setq mu4e-maildir-shortcuts
      '((:maildir "/redhat/Inbox"     :key  ?i)
        (:maildir "/redhat/[Gmail]/Sent Mail"   :key  ?s)
        (:maildir "/redhat/[Gmail]/Drafts"   :key  ?d)
        (:maildir "/redhat/kernel-core-qe"   :key  ?c)
        (:maildir "/redhat/kernel-info"      :key  ?n)
        (:maildir "/redhat/kernel-rt-qe"     :key  ?r)
        (:maildir "/redhat/tool-gitlab"      :key  ?g)))

;; (require 'mu4e-thread-folding)

;; (add-to-list 'mu4e-header-info-custom
;;              '(:empty . (:name "Empty"
;;                          :shortname ""
;;                          :function (lambda (msg) "  "))))
;; (setq mu4e-headers-fields '((:empty         .    2)
;;                             (:human-date    .   12)
;;                             (:flags         .    6)
;;                             (:mailing-list  .   10)
;;                             (:from          .   22)
;;                             (:subject       .   nil)))

;; (define-key mu4e-headers-mode-map (kbd "<tab>")     'mu4e-headers-toggle-at-point)
;; (define-key mu4e-headers-mode-map (kbd "<left>")    'mu4e-headers-fold-at-point)
;; (define-key mu4e-headers-mode-map (kbd "<S-left>")  'mu4e-headers-fold-all)
;; (define-key mu4e-headers-mode-map (kbd "<right>")   'mu4e-headers-unfold-at-point)
;; (define-key mu4e-headers-mode-map (kbd "<S-right>") 'mu4e-headers-unfold-all)

(require 'smtpmail)
;; (setq smtpmail-debug-info t)
;; (setq smtpmail-debug-verb t)
(setq smtpmail-stream-type 'ssl) ;; Don't use `starttls'
(setq message-send-mail-function 'smtpmail-send-it
   starttls-use-gnutls t
   smtpmail-starttls-credentials '(("smtp.gmail.com" 465 nil nil))
   smtpmail-auth-credentials (expand-file-name "~/.authinfo.gpg")
   smtpmail-smtp-server "smtp.gmail.com"
   smtpmail-smtp-service 465)

;; don't save messages to Sent Messages, Gmail/IMAP takes care of this
(setq mu4e-sent-messages-behavior 'delete)

;; autoupdate and index
(setq mu4e-update-interval 600)

(provide 'init-mu4e)
;;; init-mu4e.el ends here
