;;; init-misc.el --- Miscellaneous configurations -*- lexical-binding: t -*-
;;; Commentary:

;; Yet to be separated in standalone config files


;;; Code:

(global-hl-todo-mode)

;; Smooth scrolling using mouse
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-step 1) ;; keyboard scroll one line at a time

;; Set globalized column width to 80
(setq-default fill-column 80)

;; Opacity already set in `init-gui-frame.el'

;; Customized key bindings
(global-set-key (kbd "C-c C-e") 'eval-region)
(global-set-key (kbd "C-c C-b") 'eval-buffer)


(provide 'init-misc)
;;; init-misc.el ends here
