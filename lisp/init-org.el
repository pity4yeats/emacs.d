;;; init-org.el --- Configurations for org-mode -*- lexical-binding: t -*-
;;; Commentary:

;;; Code:

(when *is-a-mac* (maybe-require-package 'grab-mac-link))

(maybe-require-package 'org-cliplink)
(require-package 'org-modern)

(define-key global-map (kbd "C-c l") 'org-store-link)
(define-key global-map (kbd "C-c a") 'org-agenda)

;; Enable orgmode template shortcuts.
(require 'org-tempo)

;; Make org-mode fancier
(add-hook 'org-mode-hook 'org-indent-mode)
(global-org-modern-mode)

;; Use per-file visibility setting instead of a global setting
;; (setq org-inhibit-startup t)


;; Various preferences inherited from purcell's config
(setq org-log-done t
      org-edit-timestamp-down-means-later t
      org-hide-emphasis-markers nil
      org-catch-invisible-edits 'show
      org-export-coding-system 'utf-8
      org-fast-tag-selection-single-key 'expert
      org-html-validation-link nil
      org-export-kill-product-buffer-when-displayed t
      org-tags-column 80
      org-support-shift-select t
      org-hide-block-startup t)


;; Lots of stuff from http://doc.norang.ca/org-mode.html

;; Re-align tags when window shape changes
(with-eval-after-load 'org-agenda
  (add-hook 'org-agenda-mode-hook
            (lambda () (add-hook 'window-configuration-change-hook
                                 'org-agenda-align-tags nil t))))



;; Getting thing done with org-mode!

(global-set-key (kbd "C-c c") 'org-capture)
(setq org-default-notes-file "~/notes/agenda/agenda_inbox.org")
(setq org-capture-templates
      '(("c" "[GTD] Inbox" entry
         ;; Collects all incoming items which will then be clarified & refiled
         (file+headline "~/notes/agenda/agenda_inbox.org" "Items")
         "\n** TODO %i%? \n\n")
        ("r" "[GTD] Reminders" entry
         ;; Haven't really figured out what to do with this, but it seems to be
         ;; in everyone's config
         (file+headline "~/notes/agenda/agenda_reminder.org" "Items")
         "\n** TODO %i%? \n %U")
        ("p" "[GTD] Projects" entry
         ;; Used as a meta projects file, storing all kinds of projects, each of
         ;; which will have a tracker file in the "./projects" folder or other
         ;; dedicated folder.
         (file+headline "~/notes/agenda/agenda_project.org" "Items")
         "\n** PROJECT [/] %i%? \n %U")))

(setq org-refile-targets
      '(("~/notes/agenda/agenda_inbox.org" :maxlevel . 5)
        ("~/notes/agenda/agenda_project.org" :maxlevel . 5)
        ("~/notes/agenda/agenda_reminder.org" :maxlevel . 5)
        ("~/notes/agenda/projects/*.org" :maxlevel . 5)))

(setq org-agenda-files
      '("~/notes/agenda/agenda_inbox.org"
        "~/notes/agenda/agenda_project.org"
        "~/notes/agenda/agenda_reminder.org"
        "~/notes/bug/bug_radar.org"
        "~/notes/agenda/projects"))

(setq org-archive-location "%s_archive::* Archive")



;;; TODO settings

(setq org-todo-keywords
      (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!/!)")
              (sequence "PROJECT(p)" "|" "DONE(d!/!)" "CANCELLED(c@/!)")
              (sequence "WAITING(w@/!)" "DELEGATED(l!)" "HOLD(h)" "|"
                        "CANCELLED(c@/!)")
              ;; Bugzilla workflow
              (sequence "NEW(e)" "ASSIGNED(a)" "POST(P)" "MODIFIED(m)"
                        "Verified:Tested(t)" "ON_QA(o)" "FailedQA(f)"
                        "VERIFIED(v)")))
      org-todo-repeat-to-state "NEXT")

(setq org-todo-keyword-faces
      (quote (("NEXT" :inherit warning)
              ("PROJECT" :inherit font-lock-string-face)
              ("NEW" . "blue") ("ASSIGNED" . "purple")
              ("POST" . "pink") ("MODIFIED" . "red"))))


;;; Agenda views

(setq-default org-agenda-clockreport-parameter-plist '(:link t :maxlevel 3))


(let ((active-project-match "-INBOX/PROJECT"))

  (setq org-stuck-projects
        `(,active-project-match ("NEXT")))

  (setq org-agenda-compact-blocks t
        org-agenda-sticky t
        org-agenda-start-on-weekday nil
        org-agenda-span 'day
        org-agenda-include-diary nil
        org-agenda-sorting-strategy
        '((agenda habit-down time-up user-defined-up effort-up category-keep)
          (todo category-up effort-up)
          (tags category-up effort-up)
          (search category-up))
        org-agenda-window-setup 'current-window
        org-agenda-custom-commands
        `(("N" "Notes" tags "NOTE"
           ((org-agenda-overriding-header "Notes")
            (org-tags-match-list-sublevels t)))
          ("g" "GTD"
           ((agenda "" nil)
            (tags-todo "inbox"
                  ((org-agenda-overriding-header "INBOX")
                   (org-tags-match-list-sublevels nil)))
            ;; (stuck ""
            ;;        ((org-agenda-overriding-header "STUCK PROJECTS")
            ;;         (org-agenda-tags-todo-honor-ignore-options t)
            ;;         (org-tags-match-list-sublevels t)
            ;;         (org-agenda-todo-ignore-scheduled 'future)))
            (tags-todo "-inbox"
                       ((org-agenda-overriding-header "NEXT")
                        (org-agenda-tags-todo-honor-ignore-options t)
                        (org-agenda-todo-ignore-scheduled 'future)
                        (org-agenda-skip-function
                         '(lambda ()
                            (or (org-agenda-skip-subtree-if 'todo '("HOLD" "WAITING"))
                                (org-agenda-skip-entry-if 'nottodo '("NEXT")))))
                        (org-tags-match-list-sublevels t)
                        (org-agenda-sorting-strategy
                         '(todo-state-down effort-up category-keep))))
            (tags-todo ,active-project-match
                       ((org-agenda-overriding-header "PROJECTS")
                        (org-tags-match-list-sublevels t)
                        (org-agenda-sorting-strategy
                         '(category-keep))))
            (tags-todo "-inbox/-NEXT"
                       ((org-agenda-overriding-header "ORPHANS")
                        (org-agenda-tags-todo-honor-ignore-options t)
                        (org-agenda-todo-ignore-scheduled 'future)
                        (org-agenda-skip-function
                         '(lambda ()
                            (or (org-agenda-skip-subtree-if 'todo '("PROJECT" "HOLD" "WAITING" "DELEGATED"))
                                (org-agenda-skip-subtree-if 'nottododo '("TODO")))))
                        (org-tags-match-list-sublevels t)
                        (org-agenda-sorting-strategy
                         '(category-keep))))
            (tags-todo "/WAITING"
                       ((org-agenda-overriding-header "WAITING")
                        (org-agenda-tags-todo-honor-ignore-options t)
                        (org-agenda-todo-ignore-scheduled 'future)
                        (org-agenda-sorting-strategy
                         '(category-keep))))
            (tags-todo "/DELEGATED"
                       ((org-agenda-overriding-header "DELEGATED")
                        (org-agenda-tags-todo-honor-ignore-options t)
                        (org-agenda-todo-ignore-scheduled 'future)
                        (org-agenda-sorting-strategy
                         '(category-keep))))
            (tags-todo "-inbox"
                       ((org-agenda-overriding-header "ON-HOLD")
                        (org-agenda-skip-function
                         '(lambda ()
                            (or (org-agenda-skip-subtree-if 'todo '("WAITING"))
                                (org-agenda-skip-entry-if 'nottodo '("HOLD")))))
                        (org-tags-match-list-sublevels nil)
                        (org-agenda-sorting-strategy
                         '(category-keep))))
            ;; (tags-todo "-NEXT"
            ;;            ((org-agenda-overriding-header "All other TODOs")
            ;;             (org-match-list-sublevels t)))
            )))))


(add-hook 'org-agenda-mode-hook 'hl-line-mode)


;;; org-clock settings

;; Save the running clock and all clock history when exiting Emacs, load it on startup
(with-eval-after-load 'org
  (org-clock-persistence-insinuate))
(setq org-clock-persist t)
(setq org-clock-in-resume t)

;; Save clock data and notes in the LOGBOOK drawer
(setq org-clock-into-drawer t)
;; Save state changes in the LOGBOOK drawer
(setq org-log-into-drawer t)
;; Removes clocked tasks with 0:00 duration
(setq org-clock-out-remove-zero-time-clocks t)

;; Show clock sums as hours and minutes, not "n days" etc.
(setq org-time-clocksum-format
      '(:hours "%d" :require-hours t :minutes ":%02d" :require-minutes t))

(provide 'init-org)
;;; init-org.el ends here
