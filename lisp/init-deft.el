;;; init-deft.el --- Deft configuration file -*- lexical-binding: t -*-
;;; Commentary:

;;; Code:

(require-package 'deft)
(require 'deft)

(setq deft-default-extension "org")
(setq deft-use-filename-as-title nil)
(setq deft-use-filter-string-for-filename t)
(setq deft-file-naming-rules '((noslash . "-")
                               (nospace . "-")
                               (case-fn . downcase)))
(setq deft-text-mode 'org-mode)
(setq deft-extensions '("txt" "tex" "org" "md"))
(setq deft-directory "~/notes")
(setq deft-recursive t)

(global-set-key (kbd "C-M-d") 'deft)
(global-set-key (kbd "C-x C-g") 'deft-find-file)

(provide 'init-deft)
;;; init-deft.el ends here
